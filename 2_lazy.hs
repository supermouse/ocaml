a :: [String]
a = [[x] | x <- ['A' .. 'E']]

solve :: [String] -> [String]
solve xs = infinte xs
  where
    infinte ys = ys ++ infinte [x ++ y | x <- xs, y <- ys]