open System
open System.IO
open System.Text

type input =
    { pos_in: unit -> int
      seek_in: int -> unit
      input_char: unit -> char
    //   input_char_option: unit -> char option
      in_channel_length: int }

let input_of_string (s: string) =
    let pos = ref 0

    { pos_in = fun () -> !pos
      seek_in =
          (fun p ->
              if p < 0 then
                  invalidArg "p" "p"
              else
                  pos := p)
      input_char =
          (fun () ->
              if !pos > String.length s - 1 then
                  raise (new EndOfStreamException())
              else
                  let c = s.[!pos]
                  pos := !pos + 1
                  c)
      in_channel_length = String.length s }

let rewind i = i.seek_in (i.pos_in () - 1)

let is_non_letter x =
    " !().,;:" |> String.exists (fun c -> c = x)

let rec skip_characters i =
    if is_non_letter (i.input_char ()) then
        skip_characters i
    else
        rewind i

let rec collect_characters (b: StringBuilder) i =
    let c =
        try
            Some(i.input_char ())
        with :? EndOfStreamException -> None

    match c with
    | None -> b.ToString()
    | Some c ->
        if is_non_letter c then
            c.ToString()
        else
            b.Append(c) |> ignore
            collect_characters b i

let read_word (i: input) =
    try
        skip_characters i
        Some(collect_characters (new StringBuilder()) i)
    with :? EndOfStreamException -> None

let rec read_words_inner (i: input) (a: string list) =
    match read_word i with
    | None -> List.rev (List.map (fun (s: string) -> s.ToLower()) a)
    | Some w -> read_words_inner i (w :: a)

let read_words i = read_words_inner i []



type output =
    { output_char: char -> unit
      out_channel_length: unit -> int }

let output_of_string (s: char array) : output =
    let pos = ref 0

    { output_char =
          (fun c ->
              if !pos < Array.length s then
                  s.[!pos] <- c
                  pos := !pos + 1
              else
                  raise (new EndOfStreamException()))
      out_channel_length = (fun () -> Array.length s) }

let output_int_list (o: output) (ls: int list) =
    o.output_char '['

    ls
    |> List.iter
        (fun n ->
            n.ToString() |> String.iter o.output_char
            o.output_char ';'
            o.output_char ' ')

    o.output_char ']'


// read_words (input_of_string "t abb ccc")

let input_of_chars (cs: char array) =
    let pos = ref 0

    { pos_in = fun () -> !pos
      seek_in =
          (fun p ->
              if p < 0 then
                  invalidArg "p" "p"
              else
                  pos := p)
      input_char =
          (fun () ->
              if !pos > Array.length cs - 1 then
                  raise (new EndOfStreamException())
              else
                  let c = cs.[!pos]
                  pos := !pos + 1
                  c)
      in_channel_length = Array.length cs }

let input_string (i: input) n =
    let sb = new StringBuilder()

    let rec inner n =
        if n = 0 then
            sb.ToString()
        else
            try
                sb.Append(i.input_char ()) |> ignore
                inner (n - 1)
            with :? EndOfStreamException -> sb.ToString()

    inner n
