module Tuples =
    type 'a point =
        { x: float
          y: float
          label: string
          mutable content: 'a }

    let make_point x y l c =
        { x = x; y = y; label = l; content = c }

    let string_of_point p = sprintf "%s=(%f,%f)" p.label p.x p.y

    let string_of_point' { label = label; x = x; y = y } = sprintf "%s=(%f,%f)" label x y

    let relabel p l = { p with label = l }
    let mirror p = { p with x = p.y; y = p.x }

module Questions =
    type t<'a, 'b, 'c> =
        { a: 'a
          b: 'a
          c: 'b
          d: 'b
          e: 'c
          f: 'c }
