open System

[<AutoOpen>]
module Fold =
    let rec fold_left f a l =
        match l with
        | [] -> a
        | h :: t -> fold_left f (f a h) t

    let rec fold_right f l a =
        match l with
        | [] -> a
        | h :: t -> f h (fold_right f t a)

    let all l = fold_left (&&) true l
    let any l = fold_left (||) false l

    let setify l =
        fold_left (fun a e -> if List.contains e a then a else e :: a) [] l

    let map f l = fold_right (fun e a -> f e :: a) l []

    let copy l = fold_right (fun e a -> e :: a) l []

    let append x y = fold_right (fun e a -> e :: a) x y

    let split l =
        fold_right (fun (x, y) (xs, ys) -> (x :: xs, y :: ys)) l ([], [])

    let concat l = fold_left (@) [] l

[<AutoOpen>]
module Tree =
    type 'a tree =
        | Lf
        | Br of 'a * 'a tree * 'a tree

    let rec fold_tree f e t =
        match t with
        | Lf -> e
        | Br (x, l, r) -> f x (fold_tree f e l) (fold_tree f e r)

    let tree_size t = fold_tree (fun _ l r -> 1 + l + r) 0 t
    let tree_sum t = fold_tree (fun x l r -> x + l + r) 0 t

    let tree_preolder t =
        fold_tree (fun x l r -> [ x ] @ l @ r) [] t

    let tree_inolder t =
        fold_tree (fun x l r -> l @ [ x ] @ r) [] t

    let tree_postolder t =
        fold_tree (fun x l r -> l @ r @ [ x ]) [] t

[<AutoOpen>]
module Questions =
    let deduct = List.fold (-)

    let length l = fold_left (fun a e -> 1 + a) 0 l

    let revise l = fold_left (fun a e -> e :: a) [] l

    let mem item l =
        fold_left (fun a e -> (item = e) || a) false l

    let sentence (l: string list) =
        fold_right (fun e a -> if a = "" then e else e + " " + a) l ""

    let last l =
        match l with
        | [] -> None
        | x :: xs -> Some(fold_left (fun _ e -> e) x xs)

    let tree_depth t =
        fold_tree (fun _ l r -> 1 + max l r) 0 t

let t =
    Br(1, Br(0, Lf, Lf), Br(6, Br(4, Lf, Lf), Lf))

tree_depth t

sentence [ "a"; "b"; "c" ]
mem 4 [ 1; 2; 3 ]
length [ 1; 2; 3 ]
revise [ 1; 2; 3 ]

tree_size (t)
tree_sum t
copy [ 1; 2; 3 ]
append [ 1; 2; 3 ] [ 2; 3; 4 ]

fold_left (+) 0 [ 1; 2; 3 ]
fold_left max Int32.MinValue [ 2; 3; 4 ]

split [ (1, 2); (3, 4) ]
