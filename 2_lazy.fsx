[<AutoOpen>]
module LazyList =
    type 'a lazylist = Cons of 'a * (unit -> 'a lazylist)
    let rec lseq n = Cons(n, (fun () -> lseq (n + 1)))
    let lhd (Cons (n, _)) = n
    let ltl (Cons (_, tf)) = tf ()

    let rec ltake (Cons (h, tf)) n =
        match n with
        | 0 -> []
        | _ -> h :: ltake (tf ()) (n - 1)

    let rec ldrop (Cons (h, tf) as ll) n =
        match n with
        | 0 -> ll
        | _ -> ldrop (tf ()) (n - 1)

    let rec lmap f (Cons (h, tf)) = Cons(f h, (fun () -> lmap f (tf ())))

    let rec lfilter f (Cons (h, tf)) =
        if f h then
            Cons(h, (fun () -> lfilter f (tf ())))
        else
            lfilter f (tf ())

    let rec mkprime (Cons (h, tf)) =
        Cons(h, (fun () -> mkprime (lfilter (fun x -> x % h <> 0) (tf ()))))

    let primes = mkprime (lseq 2)

    let rec interlevave (Cons (h, tf)) l =
        Cons(h, (fun () -> interlevave l (tf ())))

    let rec lconst n = Cons(n, (fun () -> lconst n))

    let rec allfrom l =
        Cons(l, (fun () -> interlevave (allfrom (0 :: l)) (allfrom (1 :: l))))

    let allones = allfrom []


[<AutoOpen>]
module Questions =
    open System
    // let powers =
    //     lseq 0
    //     |> lmap (fun x -> 2.0**x)

    let rec ldouble n = Cons(n, (fun () -> ldouble (n * 2)))
    let thedoubles = ldouble 1

    ltake thedoubles 10

    let rec lnth (Cons (x, tf)) n =
        if n = 0 then
            x
        else
            lnth (tf ()) (n - 1)

    let to_lazy_list l =
        let rec inner left =
            match left with
            | [] -> invalidArg "left" "left"
            | [ x ] -> Cons(x, (fun () -> inner l))
            | x :: xs -> Cons(x, (fun () -> inner xs))

        inner l

    // 0 1 1 2 3 5 8 ....
    let rec fibonacci_inner x y =
        Cons(x, (fun () -> fibonacci_inner y (x + y)))

    let fibonacci = fibonacci_inner 0 1

    // 0 1 2 3 4 5 6 7
    // 1. 0 2 4 6 ...
    // 2. 1 3 5 7 ...

    // let rec interlevave (Cons (h, tf)) l =
    //     Cons(h, (fun () -> interlevave l (tf ())))
    let rec unleave (Cons (h, tf)) =
        let (Cons (h', tf')) = tf ()
        let t = tf' ()
        (Cons(h, (fun () -> fst (unleave t))), Cons(h', (fun () -> snd (unleave t))))

    let alphaumneric = "ABCDE"

    // let rec lazyA_inner acc = 
        

    // let rec allfrom l =
    //     Cons(l, (fun () -> interlevave (allfrom (0 :: l)) (allfrom (1 :: l))))

    // let allones = allfrom []


ltake fibonacci 10

ltake (to_lazy_list [ 1; 2; 3 ]) 10
// ltake powers  20
ltake allones 20

let interlevaved = interlevave (lconst 0) (lconst 1)
ltake interlevaved 10

let cubes =
    lseq 1
    |> lmap (fun x -> x * x * x)
    |> lfilter (fun x -> x % 5 = 0)




ltake (interlevave (lseq 1) (lseq 100)) 10

ltake primes 10

ltake cubes 20

lseq 0
